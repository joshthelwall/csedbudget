package me.csed17.budget;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.UUID;

import me.csed17.budget.model.transactions.Transaction;
import me.csed17.budget.monzo.MonzoAuthenticator;
import me.csed17.budget.monzo.RefreshWorker;
import me.csed17.budget.ui.MonzoViewModel;
import me.csed17.budget.ui.transactions.TransactionAddDialog;
import me.csed17.budget.ui.transactions.TransactionsViewModel;

import static me.csed17.budget.Constants.LOG_TAG;
import static me.csed17.budget.Constants.REDIRECT_URI;

public class MainActivity extends AppCompatActivity implements TransactionAddDialog.TransactionAddDialogListener {

    private MonzoViewModel monzoViewModel;

    private TransactionsViewModel transactionsViewModel;

    private MonzoAuthenticator monzoAuthenticator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "Main created");

        monzoAuthenticator = new MonzoAuthenticator();

        monzoViewModel = new ViewModelProvider(this).get(MonzoViewModel.class);

        monzoViewModel.getAccessTokenData().setValue(getPreferences(Context.MODE_PRIVATE).getString("ACCESS_TOKEN", ""));
        monzoViewModel.getRefreshTokenData().setValue(getPreferences(Context.MODE_PRIVATE).getString("REFRESH_TOKEN", ""));
        monzoViewModel.getExpiresInSecondsData().setValue(getPreferences(Context.MODE_PRIVATE).getInt("EXPIRES_IN", -1));

        monzoViewModel.getAccessTokenData().observe(this, newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                Log.d(LOG_TAG, "Access token " + newToken);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("ACCESS_TOKEN", newToken);
                editor.apply();
            }
        });

        monzoViewModel.getRefreshTokenData().observe(this, newToken -> {
            if (newToken != null && !newToken.isEmpty()) {
                Log.d(LOG_TAG, "Refresh token " + newToken);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("REFRESH_TOKEN", newToken);
                editor.apply();
            }
        });

        monzoViewModel.getExpiresInSecondsData().observe(this, newExpiresIn -> {
            if (newExpiresIn != null) {
                Log.d(LOG_TAG, "Expires in " + newExpiresIn);
                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putInt("EXPIRES_IN", newExpiresIn);
                editor.apply();

                if (newExpiresIn != -1) {
                    Log.d("Budget", "Starting Refresh in " + newExpiresIn);
                    startRefreshWorker();
                }
            }
        });

        transactionsViewModel = new ViewModelProvider(this).get(TransactionsViewModel.class);

        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home,
                R.id.navigation_transactions
        ).build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(LOG_TAG, getIntent().toString());
        Log.d(LOG_TAG, "Access:" + monzoViewModel.getAccessTokenData().getValue());
        Log.d(LOG_TAG, String.valueOf(monzoViewModel.getIsAuthenticatingData().getValue()));
        Log.d(LOG_TAG, String.valueOf(monzoViewModel.getIsAuthenticatedData().getValue()));

        handleAuthenticationIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleAuthenticationIntent(intent);
    }

    private void handleAuthenticationIntent(Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_VIEW)
                && intent.getData() != null
                && Objects.equals(intent.getData().getHost(), Uri.parse(REDIRECT_URI).getHost())
                && !monzoViewModel.getIsAuthenticatedData().getValue()) {
            String authCode = intent.getData().getQueryParameter("code");
            if (authCode == null) return;

            Log.d(LOG_TAG, "Auth code:" + authCode);

            monzoAuthenticator.retrieveAccessResponse(authCode,
                    authResponse -> {
                        Log.d(LOG_TAG, "Response!");

                        monzoViewModel.getAccessTokenData().postValue(authResponse.getAccessToken());
                        monzoViewModel.getRefreshTokenData().postValue(authResponse.getRefreshToken());
                        monzoViewModel.getExpiresInSecondsData().postValue(authResponse.getExpiresInSeconds());

                        runOnUiThread(() -> Toast.makeText(MainActivity.this, R.string.monzo_success_msg, Toast.LENGTH_LONG).show());

                    },
                    ex -> {
                        String errorMessage = getString(ex == null ? R.string.authentication_error : R.string.network_error);

                        if (ex != null) ex.printStackTrace();

                        runOnUiThread(() -> Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show());

                        monzoViewModel.getIsAuthenticatingData().postValue(false);
                    });
        }
    }

    private void startRefreshWorker() {
        Data data = new Data.Builder().putString("REFRESH_TOKEN", monzoViewModel.getRefreshTokenData().getValue()).build();
        OneTimeWorkRequest refreshRequest = new OneTimeWorkRequest.Builder(RefreshWorker.class).setInputData(data).setInitialDelay(Duration.ofSeconds(monzoViewModel.getExpiresInSecondsData().getValue())).build();

        WorkManager.getInstance(this).enqueue(refreshRequest);

        WorkManager.getInstance(this).getWorkInfoByIdLiveData(refreshRequest.getId()).observe(this, info -> {
            if (info != null && info.getState().isFinished()) {
                String ACCESS_TOKEN = info.getOutputData().getString("ACCESS_TOKEN");
                String REFRESH_TOKEN = info.getOutputData().getString("REFRESH_TOKEN");
                int EXPIRES_IN = info.getOutputData().getInt("EXPIRES_IN", -1);

                monzoViewModel.getAccessTokenData().postValue(ACCESS_TOKEN);
                monzoViewModel.getRefreshTokenData().postValue(REFRESH_TOKEN);
                monzoViewModel.getExpiresInSecondsData().postValue(EXPIRES_IN);

                SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
                editor.putString("ACCESS_TOKEN", ACCESS_TOKEN);
                editor.putString("REFRESH_TOKEN", REFRESH_TOKEN);
                editor.putInt("EXPIRES_IN", EXPIRES_IN);
                editor.apply();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(LOG_TAG, "Main destroyed");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, EditText amountEditText, EditText notesEditText, DatePicker datePicker, TimePicker timePicker) {
        if (amountEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Transaction was not added, as amount wasn't specified.", Toast.LENGTH_SHORT).show();
            return;
        }

        int amount = new BigDecimal(amountEditText.getText().toString()).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP).intValue();
        String notes = notesEditText.getText().toString();
        Instant dateTime = LocalDateTime.of(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth(), timePicker.getHour(), timePicker.getMinute()).toInstant(ZoneOffset.UTC);
        if (dateTime.truncatedTo(ChronoUnit.MINUTES).equals(Instant.now().truncatedTo(ChronoUnit.MINUTES))) {
            dateTime = LocalDateTime.of(datePicker.getYear(), datePicker.getMonth() + 1, datePicker.getDayOfMonth(), timePicker.getHour(), timePicker.getMinute(), LocalDateTime.now().getSecond()).toInstant(ZoneOffset.UTC);
        }

        Log.d("Budget", dateTime.toString());

        String created = DateTimeFormatter.ISO_INSTANT.format(dateTime);

        transactionsViewModel.addLocalTransaction(new Transaction(UUID.randomUUID().toString(), "GBP", amount, notes, created));
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }
}
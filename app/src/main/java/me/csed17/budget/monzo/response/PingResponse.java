package me.csed17.budget.monzo.response;

public class PingResponse {
    private final boolean authenticated;

    public PingResponse(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }
}

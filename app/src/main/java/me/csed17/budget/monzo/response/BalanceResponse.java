package me.csed17.budget.monzo.response;

public class BalanceResponse {
    private final int balance;

    public BalanceResponse(int balance) {
        this.balance = balance;
    }

    public int getBalancePence() {
        return balance;
    }
}

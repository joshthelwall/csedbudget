package me.csed17.budget.monzo.response;

import java.util.List;

import me.csed17.budget.model.transactions.Transaction;

public class TransactionsResponse {
    private final List<Transaction> transactions;

    public TransactionsResponse(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

}

package me.csed17.budget.model.transactions;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TransactionBuilder {
    private String id;
    private String currency;
    private int amount;
    private String notes;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    private boolean cancelled = false;

    public void cancel() {
        this.cancelled = true;
    }

    public TransactionBuilder id(String id) {
        this.id = id;
        return this;
    }

    public TransactionBuilder currency(String currency) {
        this.currency = currency;
        return this;
    }

    public TransactionBuilder amount(int amount) {
        this.amount = amount;
        return this;
    }

    public TransactionBuilder notes(String notes) {
        this.notes = notes;
        return this;
    }

    public TransactionBuilder year(int year) {
        this.year = year;
        return this;
    }

    public TransactionBuilder month(int month) {
        this.month = month;
        return this;
    }

    public TransactionBuilder day(int day) {
        this.day = day;
        return this;
    }

    public TransactionBuilder hour(int hour) {
        this.hour = hour;
        return this;
    }

    public TransactionBuilder minute(int minute) {
        this.minute = minute;
        return this;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public Transaction build() {
        if (this.cancelled) return null;

        return new Transaction(id, currency, amount, notes, LocalDateTime.of(year, month, day, hour, minute).format(DateTimeFormatter.ISO_INSTANT));
    }
}


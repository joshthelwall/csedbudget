package me.csed17.budget.model.transactions;

import java.time.ZonedDateTime;
import java.util.Objects;

public class Transaction {
    private final String id;
    private final String currency;
    private final int amount;
    private final String notes;
    private final String created;
    private transient ZonedDateTime utcDateTime;

    public Transaction(String id, String currency, int amount, String notes, String created) {
        this.id = id;
        this.currency = currency;
        this.amount = amount;
        this.notes = notes;
        this.created = created;
    }

    public ZonedDateTime getDateTime() {
        if (utcDateTime == null) {
            utcDateTime = ZonedDateTime.parse(created);
        }

        return utcDateTime;
    }

    public int getAmount() {
        return amount;
    }

    public String getId() {
        return id;
    }

    public String getNotes() {
        return notes;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

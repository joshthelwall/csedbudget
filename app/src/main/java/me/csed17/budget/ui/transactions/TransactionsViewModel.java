package me.csed17.budget.ui.transactions;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import me.csed17.budget.Constants;
import me.csed17.budget.model.transactions.Transaction;
import me.csed17.budget.monzo.response.TransactionsResponse;
import me.csed17.budget.ui.MonzoViewModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static me.csed17.budget.Constants.LOG_TAG;

public class TransactionsViewModel extends ViewModel {

    private final OkHttpClient client = new OkHttpClient();
    private final Moshi moshi = new Moshi.Builder().build();
    private final JsonAdapter<TransactionsResponse> transactionJsonAdapter = moshi.adapter(TransactionsResponse.class);

    private List<Transaction> localTransactions = new ArrayList<>();
    private List<Transaction> remoteTransactions = new ArrayList<>();

    private MutableLiveData<List<Transaction>> transactions = new MutableLiveData<>(new ArrayList<>());

    private MonzoViewModel monzoViewModel = null;

    public void setMonzoViewModel(MonzoViewModel monzoViewModel) {
        this.monzoViewModel = monzoViewModel;
    }

    public void addLocalTransaction(Transaction transaction) {
        localTransactions.add(transaction);

        transactions.postValue(Stream.concat(remoteTransactions.stream(), localTransactions.stream()).sorted((o1, o2) -> -o1.getDateTime().compareTo(o2.getDateTime())).collect(Collectors.toList()));
        // Save local transactions
    }

    public void updateRemoteTransactions() {
        if (monzoViewModel == null || !monzoViewModel.getIsAuthenticatedData().getValue()) {
            return;
        }

        Request req = new Request.Builder()
                .url(HttpUrl.parse("https://api.monzo.com/transactions")
                        .newBuilder()
                        .addQueryParameter("account_id", Constants.ACCOUNT_ID)
                        .addQueryParameter("since", DateTimeFormatter.ISO_INSTANT.format(Instant.now().minus(Duration.ofDays(89)))).build())
                .header("Authorization", "Bearer " + monzoViewModel.getAccessTokenData().getValue())
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                List<Transaction> transactionsResponse = transactionJsonAdapter.fromJson(response.body().source()).getTransactions();
                if (transactionsResponse == null) {
                    return;
                }

                Collections.reverse(transactionsResponse);

                Log.d(LOG_TAG, transactionsResponse.toString());

                response.close();

                remoteTransactions = transactionsResponse;

                transactions.postValue(Stream.concat(remoteTransactions.stream(), localTransactions.stream()).sorted((o1, o2) -> -o1.getDateTime().compareTo(o2.getDateTime())).collect(Collectors.toList()));
            }
        });
    }

    public MutableLiveData<List<Transaction>> getTransactions() {
        return transactions;
    }
}
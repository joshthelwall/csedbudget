package me.csed17.budget.ui.transactions;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import me.csed17.budget.databinding.FragmentTransactionsBinding;
import me.csed17.budget.ui.MonzoFragment;

import static me.csed17.budget.Constants.LOG_TAG;

public class TransactionsFragment extends MonzoFragment {

    private FragmentTransactionsBinding binding;

    private TransactionsViewModel transactionsViewModel;

    private TransactionsAdapter transactionsAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentTransactionsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        transactionsViewModel = new ViewModelProvider(requireActivity()).get(TransactionsViewModel.class);
        transactionsViewModel.setMonzoViewModel(monzoViewModel);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        binding.transactionsRecyclerView.setLayoutManager(layoutManager);

        transactionsAdapter = new TransactionsAdapter();

        transactionsViewModel.getTransactions().observe(getViewLifecycleOwner(), l -> {
            Log.d(LOG_TAG, l.toString());
            transactionsAdapter.submitList(l);

            if (l.isEmpty()) {
                binding.transactionsRecyclerView.setVisibility(View.GONE);
                binding.transactionsPlaceholderText.setVisibility(View.VISIBLE);
            } else {
                binding.transactionsRecyclerView.setVisibility(View.VISIBLE);
                binding.transactionsPlaceholderText.setVisibility(View.GONE);
            }
        });

        binding.transactionsRecyclerView.setAdapter(transactionsAdapter);

        binding.addTransactionFab.setOnClickListener(v -> {
            DialogFragment transactionDialog = new TransactionAddDialog();
            transactionDialog.show(requireActivity().getSupportFragmentManager(), "transaction_time_dialog");
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        transactionsViewModel.updateRemoteTransactions();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "destroy");
        binding = null;
    }

}
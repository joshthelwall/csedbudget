package me.csed17.budget.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import java.math.BigDecimal;
import java.math.RoundingMode;

import me.csed17.budget.R;
import me.csed17.budget.databinding.FragmentHomeBinding;
import me.csed17.budget.ui.MonzoFragment;
import okhttp3.OkHttpClient;

public class HomeFragment extends MonzoFragment {

    private HomeViewModel homeViewModel;

    private FragmentHomeBinding binding;

    private final OkHttpClient client = new OkHttpClient();

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();

        homeViewModel = new ViewModelProvider(requireActivity()).get(HomeViewModel.class);
        homeViewModel.setMonzoViewModel(monzoViewModel);

        homeViewModel.getBalance().observe(getViewLifecycleOwner(), bal -> binding.balanceText.setText(getString(R.string.balance_display, bal.divide(new BigDecimal(100), RoundingMode.HALF_DOWN))));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        homeViewModel.updateBalance();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}
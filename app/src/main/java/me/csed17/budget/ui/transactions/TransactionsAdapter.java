package me.csed17.budget.ui.transactions;

import android.graphics.Color;
import android.icu.text.DecimalFormat;
import android.icu.text.NumberFormat;
import android.icu.util.Currency;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import me.csed17.budget.R;
import me.csed17.budget.model.transactions.Transaction;

public class TransactionsAdapter extends ListAdapter<Transaction, TransactionsAdapter.TransactionViewHolder> {

    protected TransactionsAdapter() {
        super(new DiffUtil.ItemCallback<Transaction>() {
            @Override
            public boolean areItemsTheSame(@NonNull Transaction oldItem, @NonNull Transaction newItem) {
                return oldItem.getId().equals(newItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Transaction oldItem, @NonNull Transaction newItem) {
                return oldItem.getAmount() == newItem.getAmount() &&
                        oldItem.getDateTime().equals(newItem.getDateTime()) &&
                        oldItem.getNotes().equals(newItem.getNotes());
            }
        });
    }


    @NonNull
    @Override
    public TransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false);

        return new TransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionViewHolder holder, int position) {
        Transaction transaction = getItem(position);

        int penceAmount = transaction.getAmount();

        BigDecimal poundsAbsoluteAmount = new BigDecimal(penceAmount).setScale(2, RoundingMode.HALF_DOWN).divide(new BigDecimal(100), RoundingMode.HALF_DOWN).abs();

        Currency currency = Currency.getInstance(transaction.getCurrency());

        NumberFormat formatter = DecimalFormat.getCurrencyInstance();
        formatter.setCurrency(currency);

        if (penceAmount >= 0) {
            holder.getAmountTextView().setText(formatter.format(poundsAbsoluteAmount));
            holder.getAmountTextView().setTextColor(Color.GREEN);
        } else {
            holder.getAmountTextView().setText(formatter.format(poundsAbsoluteAmount.negate()));
            holder.getAmountTextView().setTextColor(Color.RED);
        }

        Period difference = Period.between(transaction.getDateTime().toLocalDate(), ZonedDateTime.now().toLocalDate());

        if (Math.abs(difference.getYears()) >= 1) {
            holder.getDateTextView().setText(transaction.getDateTime().format(DateTimeFormatter.ofPattern("d L u")));
        } else if (Math.abs(difference.getDays()) >= 5) {
            holder.getDateTextView().setText(transaction.getDateTime().format(DateTimeFormatter.ofPattern("LLL d',' H:mm")));
        } else if (transaction.getDateTime().toInstant().truncatedTo(ChronoUnit.DAYS).equals(Instant.now().truncatedTo(ChronoUnit.DAYS))) {
            holder.getDateTextView().setText(transaction.getDateTime().format(DateTimeFormatter.ofPattern("'Today' H:mm")));
        } else if (transaction.getDateTime().toInstant().truncatedTo(ChronoUnit.DAYS).equals(Instant.now().minus(Period.ofDays(1)).truncatedTo(ChronoUnit.DAYS))) {
            holder.getDateTextView().setText(transaction.getDateTime().format(DateTimeFormatter.ofPattern("'Y''Day' H:mm")));
        } else {
            holder.getDateTextView().setText(transaction.getDateTime().format(DateTimeFormatter.ofPattern("E H:mm")));
        }

        if (transaction.getNotes() != null && !transaction.getNotes().isEmpty()) {
            holder.getDescriptionTextView().setText(transaction.getNotes());
        }
    }

    public static class TransactionViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout textItemLayout;

        public TransactionViewHolder(@NonNull LinearLayout textItemLayout) {
            super(textItemLayout);
            this.textItemLayout = textItemLayout;
        }

        public TextView getAmountTextView() {
            return textItemLayout.findViewById(R.id.transaction_amount_text_view);
        }

        public TextView getDateTextView() {
            return textItemLayout.findViewById(R.id.transaction_date_text_view);
        }

        public TextView getDescriptionTextView() {
            return textItemLayout.findViewById(R.id.description_text_view);
        }
    }
}

package me.csed17.budget.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import me.csed17.budget.Constants;
import me.csed17.budget.monzo.MonzoAuthenticator;

import static me.csed17.budget.Constants.LOG_TAG;

public abstract class MonzoFragment extends Fragment {

    protected MonzoAuthenticator monzoAuthenticator;

    protected MonzoViewModel monzoViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        monzoAuthenticator = new MonzoAuthenticator();

        monzoViewModel = new ViewModelProvider(requireActivity()).get(MonzoViewModel.class);

        if (!monzoViewModel.getIsAuthenticatingData().getValue() && !monzoViewModel.getIsAuthenticatedData().getValue()) {
            Toast.makeText(requireActivity(), "Monzo account needs to be authenticated.", Toast.LENGTH_LONG).show();
            startAuthenticationIntent();
        } else {
            monzoAuthenticator.retrieveIsAuthenticated(monzoViewModel.getAccessTokenData().getValue(),
                    isAuthenticated -> {
                        if (!isAuthenticated) {
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    Toast.makeText(activity, "Monzo account needs to be re-authenticated.", Toast.LENGTH_LONG).show();
                                });
                            }

                            monzoViewModel.getIsAuthenticatedData().postValue(false);
                            startAuthenticationIntent();
                        }
                    },
                    ex -> {
                        ex.printStackTrace();
                        Log.d(LOG_TAG, "Problem checking if authenticated");
                        Activity activity = getActivity();
                        if (activity != null) {
                            activity.runOnUiThread(() -> {
                                Toast.makeText(activity, "Unable to check if Monzo account is authenticated. Data shown may be old. Try again later.", Toast.LENGTH_LONG).show();
                            });
                        }
                    });
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void startAuthenticationIntent() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://auth.monzo.com/?" + "client_id=" + Constants.CLIENT_ID + "&redirect_uri=" + Constants.REDIRECT_URI + "&response_type=code"));
        Log.d(LOG_TAG, "Intent started");
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }

        monzoViewModel.getIsAuthenticatingData().postValue(true);
    }

}

package me.csed17.budget.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import me.csed17.budget.Constants;
import me.csed17.budget.ui.MonzoViewModel;
import me.csed17.budget.monzo.response.BalanceResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<BigDecimal> balance;

    private final OkHttpClient client = new OkHttpClient();
    private final Moshi moshi = new Moshi.Builder().build();
    private final JsonAdapter<BalanceResponse> balanceResponseJsonAdapter = moshi.adapter(BalanceResponse.class);

    private MonzoViewModel monzoViewModel = null;

    public HomeViewModel() {
        balance = new MutableLiveData<>();
    }

    public void setMonzoViewModel(MonzoViewModel monzoViewModel) {
        this.monzoViewModel = monzoViewModel;
    }

    public void updateBalance() {
        if (monzoViewModel == null || !monzoViewModel.getIsAuthenticatedData().getValue()) {
            return;
        }

        Request req = new Request.Builder()
                .url(HttpUrl.parse("https://api.monzo.com/balance").newBuilder().addQueryParameter("account_id", Constants.ACCOUNT_ID).build())
                .header("Authorization", "Bearer " + monzoViewModel.getAccessTokenData().getValue())
                .build();

        client.newCall(req).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                BalanceResponse balanceResponse = balanceResponseJsonAdapter.fromJson(response.body().source());

                response.close();

                balance.postValue(new BigDecimal(balanceResponse.getBalancePence()).setScale(2, RoundingMode.HALF_DOWN));
            }

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void setBalance(BigDecimal balance) {
        this.balance.setValue(balance);
    }

    public LiveData<BigDecimal> getBalance() {
        return this.balance;
    }
}
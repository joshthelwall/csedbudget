package me.csed17.budget.ui.transactions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import me.csed17.budget.R;

public class TransactionAddDialog extends DialogFragment {

    public interface TransactionAddDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, EditText amountEditText, EditText notesEditText, DatePicker datePicker, TimePicker timePicker);

        void onDialogNegativeClick(DialogFragment dialog);
    }

    TransactionAddDialogListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (TransactionAddDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(requireActivity().toString() + "must implement TransactionAddDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = requireActivity().getLayoutInflater().inflate(R.layout.transaction_add_dialog, null);
        builder.setView(view);

        EditText amountEditText = view.findViewById(R.id.amount_edit_text);
        EditText notesEditText = view.findViewById(R.id.notes_edit_text);

        DatePicker datePicker = view.findViewById(R.id.transaction_date_picker);
        TimePicker timePicker = view.findViewById(R.id.transaction_time_picker);


        builder.setPositiveButton("Confirm", (dialog, id) -> listener.onDialogPositiveClick(TransactionAddDialog.this, amountEditText, notesEditText, datePicker, timePicker));
        builder.setNegativeButton("Cancel", (dialog, id) -> listener.onDialogNegativeClick(TransactionAddDialog.this));

        return builder.create();
    }
}
